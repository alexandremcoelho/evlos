const carteira = {
    "cem": 1,
    "cinquenta": 3,
    "vinte": 4,
    "dez": 1,
    "cinco": 3,
    "dois": 12,
    "um": 10
}


const atualiza_saque = (valor) => {
    const saque_field = document.getElementById("saque")
    if(Number(saque_field.textContent) < 10000000) {
        if(saque_field.textContent === "0" ) {
            saque_field.innerHTML = valor
        }else {
           saque_field.innerHTML = String(saque_field.textContent) + String(valor)
        }
    }
}

const reset = () => {
    const saque_field = document.getElementById("saque")
    const saldo_field = document.getElementById("saldo")
    const resposta_field = document.getElementById("resposta")

    saque_field.innerHTML = 0
    saldo_field.innerHTML = "122"
    resposta_field.innerHTML = ""
    resposta_field.style.color = "unset"

}

const apaga = () => {
    const saque_field = document.getElementById("saque")
    if( saque_field.textContent.length > 1 ) {
        saque_field.innerHTML =  String(saque_field.textContent).slice(0, -1)
    }else {
        saque_field.innerHTML = 0
    }
}

const limpa = () => {
    const saque_field = document.getElementById("saque")
    saque_field.innerHTML = 0
}

const responder = (saldo, saque, notas_retiradas, resposta) => {
    saldo.innerHTML = Number(saldo.textContent) - Number(saque.textContent)

    resposta_formatada = `saque de R$ ${saque.textContent}! <br />Foram retiradas `
    if(notas_retiradas.cem) resposta_formatada += `${notas_retiradas.cem} nota(s) de 100 reais,`
    if(notas_retiradas.cinquenta) resposta_formatada += `${notas_retiradas.cinquenta} nota(s) de 50 reais,`
    if(notas_retiradas.vinte) resposta_formatada += `${notas_retiradas.vinte} nota(s) de 20 reais,`
    if(notas_retiradas.dez) resposta_formatada += `${notas_retiradas.dez} nota(s) de 10 reais,`
    if(notas_retiradas.cinco) resposta_formatada += `${notas_retiradas.cinco} nota(s) de 5 reais,`
    if(notas_retiradas.dois) resposta_formatada += `${notas_retiradas.dois} nota(s) de 2 reais,`
    if(notas_retiradas.um) resposta_formatada += `${notas_retiradas.um} nota(s) de 1 real,`
    resposta_formatada= resposta_formatada.replace(/.$/, '!');
    

    resposta.innerHTML = resposta_formatada
    resposta.style.color = "green"
    saque.innerHTML = 0
}

const retirada = (saque, saldo, resposta) => {
    let saque_atual = Number(saque.textContent)
    const notas_retiradas = {
        "cem": 0,
        "cinquenta": 0,
        "vinte": 0,
        "dez": 0,
        "cinco": 0,
        "dois": 0,
        "um": 0
    }

    while (saque_atual >= 100 && carteira.cem > 0) {
        saque_atual-= 100
        carteira.cem-= 1
        notas_retiradas.cem+= 1
    }
    while (saque_atual >= 50 && carteira.cinquenta > 0) {
        saque_atual-= 50
        carteira.cinquenta-= 1
        notas_retiradas.cinquenta+= 1
    }
    while (saque_atual >= 20 && carteira.vinte > 0) {
        saque_atual-= 20
        carteira.vinte-= 1
        notas_retiradas.vinte+= 1
    }
    while (saque_atual >= 10 && carteira.dez > 0) {
        saque_atual-= 10
        carteira.dez-= 1
        notas_retiradas.dez+= 1
    }
    while (saque_atual >= 5 && carteira.cinco > 0) {
        saque_atual-= 5
        carteira.cinco-= 1
        notas_retiradas.cinco+= 1
    }
    while (saque_atual >= 2 && carteira.dois > 0) {
        saque_atual-= 2
        carteira.dois-= 1
        notas_retiradas.dois+= 1
    }
    while (saque_atual >= 1 && carteira.um > 0) {
        saque_atual-= 1
        carteira.um-= 1
        notas_retiradas.um+= 1
    }

    if (saque_atual > 0) {
        resposta.innerHTML = "caixa eletrônico com notas insuficientes!"
        resposta.style.color = "red"
    }else {
        responder(saldo, saque, notas_retiradas, resposta)
    }

}

const saque = () => {
    const saque_field = document.getElementById("saque")
    const saldo_field = document.getElementById("saldo")
    const resposta_field = document.getElementById("resposta")

    if(Number(saldo_field.textContent) >= Number(saque_field.textContent)){
        if (saque_field.textContent != 0) retirada(saque_field, saldo_field, resposta_field)

    }else {
        resposta_field.innerHTML = "saldo insuficiente!"
        resposta_field.style.color = "red"
    }
}